## Dobarvování černobílých filmů pomocí Konvolučních Neuronových Sítí

Stáhněte si neuronovou sít natrénovanou na dobarvování černobílých obrázků (http://richzhang.github.io/colorization/) a aplikujte ji na černobílé filmy. Zkontrolujte, že síť správně obarvuje korespondující oblasti v dvou následujících framech konzistentně (př.: obloha mezi snímky nezmění barvu z modré na zelenou) a případně implementujte algoritmus na stabilizaci barev.

* Članek: https://arxiv.org/pdf/1603.08511.pdf
* Podobný projekt: https://vimeo.com/151159255#
* Zdrojová data: https://drive.google.com/drive/folders/1IAF9nO1Zd-afT4nopjRxUCj1vKT7Rdxi


### Jak zprovoznit projekt

Je důležité mít v počítači funkční verzi frameworku Caffe.
* git clone git@gitlab.fit.cvut.cz:polcavoj/mvi-sp.git
* Spustit script ./colorization-master/colorization/models/fetch_release_models.sh, který stáhne natrénované modely pro obarvování.
* Přejít do složky /colorization-master/colorization/ a zde buď přímo spustit pomocí "python3 main.py", který použije defaultní video ze složky ../../data/video/lukas_graham_shorter.mp4 nebo zadat jako argument cestu k videu.
* Výsledek je poté ve složce ../../data/video/ - final_output_video.mkv.
